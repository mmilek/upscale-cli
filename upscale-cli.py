from sys import argv
import os
import cv2
from cv2 import dnn_superres

# upscale-cli  - a simple Python script for upscaling images from the command line. Wrapped in a Bash script for elegance.

# Syntax: upscale mode scale file

# To use a model, download it using the links below and place in the ./models/ directory.

# EDSR - https://github.com/Saafke/EDSR_Tensorflow/tree/master/models
# ESPCN - https://github.com/fannymonori/TF-ESPCN/tree/master/export
# FSRCNN - https://github.com/Saafke/FSRCNN_Tensorflow/tree/master/models
# LapSRN - https://github.com/fannymonori/TF-LapSRN/tree/master/export

folder_path = os.path.dirname(__file__)

try:
    mode = argv[1]
    scale = int(argv[2])
    image_name = argv[3]
except IndexError:
    print('Check if a proper number of arguments has been entered. Refer to "upscale -h" for help.')
    exit(1);

sr = dnn_superres.DnnSuperResImpl_create()
image = cv2.imread(f'{image_name}')

if mode == 'edsr':
    if scale == 2:
        path = f"{folder_path}/models/EDSR_x2.pb"
    if scale == 3:
        path = f"{folder_path}/models/EDSR_x3.pb"
    if scale == 4:
        path = f"{folder_path}/models/EDSR_x4.pb"

elif mode == 'espcn':
    if scale == 2:
        path = f"{folder_path}/models/ESPCN_x2.pb"
    if scale == 3:
        path = f"{folder_path}/models/ESPCN_x3.pb"
    if scale == 4:
        path = f"{folder_path}/models/ESPCN_x4.pb"

elif mode == 'lapsrn':
    if scale == 2:
        path = f"{folder_path}/models/LapSRN_x2.pb"
    if scale == 4:
        path = f"{folder_path}/models/LapSRN_x4.pb"
    if scale == 8:
        path = f"{folder_path}/models/LapSRN_x8.pb"

elif mode == 'fsrcnn':
    if scale == 2:
        path = f"{folder_path}/models/FSRCNN_x2.pb"
    if scale == 3:
        path = f"{folder_path}/models/FSRCNN_x3.pb"
    if scale == 4:
        path = f"{folder_path}/models/FSRCNN_x4.pb"

try:
    sr.readModel(path)
except NameError:
    print('An invalid scale has been entered. Check "upscale -h" for help')
    exit(1)
except cv2.error:
    print('You do not have this model in the models/ directory. Check "upscale -h" for download URLs.')
    exit(1)

sr.setModel(f"{mode}", scale)
print("Working...")
result = sr.upsample(image)
cv2.imwrite(f"./{image_name[:-4]}_upscaled.png", result)
print(f"Upscaled image written to {image_name[:-4]}_upscaled.png")
