#!/bin/bash
function usage(){
    cat <<USAGE

    upscale-cli  - a simple Python script for upscaling images from the command line. Wrapped in a Bash script for elegance. 

    Syntax: upscale-cli [mode] scale file

    Modes:
        --edsr:     Slowest inference, best results (2x, 3x, 4x)
        --espcn:    Fast inference, good results (2x, 3x, 4x)
        --fsrcnn:   Fast inference, good results (2x, 3x, 4x)
        --lapsrn:   Fast inference, good results (2x, 4x, 8x)

    Additional information:
    To use a model, download it using the links below and place in the ./models/ directory.

    EDSR - https://github.com/Saafke/EDSR_Tensorflow/tree/master/models
    ESPCN - https://github.com/fannymonori/TF-ESPCN/tree/master/export
    FSRCNN - https://github.com/Saafke/FSRCNN_Tensorflow/tree/master/models
    LapSRN - https://github.com/fannymonori/TF-LapSRN/tree/master/export

USAGE
    exit 1
}

if [ $# -eq 0 ]; then
    usage
    exit 1
fi

while [ "$1" != "" ]; do
    case $1 in
    --edsr)
        MODE='edsr'
        ;;
    --espcn)
        MODE='espcn'
        ;;
    --lapsrn)
        MODE='lapsrn'
        ;;
    --fsrcnn)
        MODE='fsrcnn'
        ;;
   -h | --help)
       usage
       ;;
   *)
       usage
       exit 1
       ;;
    esac
    shift
    SCALE=$1
    shift
    FILE=$1
    shift
done
python3 $(dirname $0)/upscale-cli.py $MODE $SCALE $FILE
